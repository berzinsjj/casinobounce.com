<footer>
    <?php $footer = get_field('footer', 11); ?>
    <div class="back-yop">
        <div class="back-to-top">
            <a href="/"><img src="\wp-content\themes\casinobounce\assets\images\backtotop.png"></a>
        </div>
    </div>
    <?php echo wp_nav_menu(array('menu' => 'Footer menu')); ?>
    <div class="footer-logo-text">
        <div class="footer-logo">
            <?php foreach ($footer['footer_icons'] as $icon) : ?>
                <a href="<?php echo $icon['link']; ?>" target="_blank">
                    <img src="<?php echo $icon['icon']; ?>" class="footer-icon">
                    <img src="<?php echo $icon['icon_hover']; ?>" class="footer-icon-hover">
                </a>
            <?php endforeach; ?>
        </div>
        <div class="footer-text">
            <p><?php echo $footer['copyright']; ?></p>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>