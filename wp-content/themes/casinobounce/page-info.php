<?php /* Template Name: Info */ ?>
<?php get_header(); ?>
<div class="header-intro-background">
    <div class="header-intro-text">
        <?php $intro_block = get_field('intro_block'); ?>
        <h1><?php echo $intro_block['title']; ?></h1>
        <p><?php echo $intro_block['description']; ?></p>
    </div>
</div>
<div class="page-container">
    <div class="page-content">
        <div class="casino-games-types-of-bonuses page-info">
            <div class="casino-games">
                <?php $page = get_post(get_the_ID()); ?>
                <h2><?php echo $page->post_title; ?></h2>
                <div class="casino-games-paragraph">
                    <p><?php echo nl2br($page->post_content); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
