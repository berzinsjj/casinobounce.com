<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
<div class="header-intro-background">
    <div class="header-intro-text">
        <?php $intro_block = get_field('intro_block'); ?>
        <h1><?php echo $intro_block['title']; ?></h1>
        <p><?php echo $intro_block['description']; ?></p>
    </div>
</div>
<div class="page-container">
    <div class="casino-box-container-background">
        <div class="casino-box-container">
            <?php $featured_casinos = get_field('featured_casinos'); ?>
            <?php foreach ($featured_casinos as $casino) : ?>
                <div class="casino-box">
                    <div class="casino-box-logo-stars">
                        <div class="casino-box-logo">
                            <a href="<?php echo get_field('affialite_link', $casino->ID); ?>"> <img src="<?php echo get_field('logo', $casino->ID); ?>"></a>
                        </div>
                        <div class="rating-and-number-container">
                            <div class="star-ratings">
                                <div class="fill-ratings" style="width: <?php echo get_field('rating', $casino->ID); ?>%">
                                    <span>★★★★★</span>
                                </div>
                                <div class="empty-ratings">
                                    <span>☆☆☆☆☆</span>
                                </div>
                            </div>
                            <p class="rating-number"><?php echo get_field('rating', $casino->ID); ?>/100</p>
                        </div>
                    </div>
                    <div class="casino-box-bonus">
                        <p><?php echo $casino->post_title; ?></p>
                        <h3><?php echo get_field('bonus', $casino->ID); ?></h3>
                        <p><?php echo get_field('free_spins', $casino->ID); ?></p>
                    </div>
                    <div class="casino-box-visit">
                        <?php foreach (get_field('tags', $casino->ID) as $tag) : ?>
                            <div class="casino-box-visit-list">
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-right" class="svg-inline--fa fa-chevron-circle-right fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm113.9 231L234.4 103.5c-9.4-9.4-24.6-9.4-33.9 0l-17 17c-9.4 9.4-9.4 24.6 0 33.9L285.1 256 183.5 357.6c-9.4 9.4-9.4 24.6 0 33.9l17 17c9.4 9.4 24.6 9.4 33.9 0L369.9 273c9.4-9.4 9.4-24.6 0-34z"></path></svg>
                            <p><?php echo $tag['tag'] ?></p>
                        </div>
                        <?php endforeach; ?>
                        <div class="visit-button">
                            <a href="<?php echo get_field('affialite_link', $casino->ID); ?>" target="_blank">VISIT CASINO</a>
                        </div>
                        <div class="terms-here">
                            <p>18+. Terms apply. Read full T&C’s</p>
                            <a href="javascript:void(0);" class="open-popachka">here</a>
                        </div>
                    </div>
                    <div id="popup" class="overlay">
                        <div class="popup">
                            <h2>Terms & conditions</h2>
                            <a class="close" href="javascript:void(0);">&times;</a>
                            <div class="content">
                                <p><?php echo get_field('terms_info', $casino->ID); ?></p>
                            </div>
                            <div class="poog">
                                <a href="<?php echo get_field('terms_link', $casino->ID); ?>" target="_blank">View full Terms and Conditions here</a>
                            </div>
                        </div>
                    </div>
                </div>
           <?php endforeach; ?>
        </div>
    </div>
    <div class="page-content">
        <div class="casino-bonuses">
            <div class="first-text">
                <?php $first_paragraph = get_field('first_paragraph'); ?>
                <?php foreach ($first_paragraph as $text) : ?>
                    <h2><?php echo $text['title']; ?></h2>
                    </div>
                    <div class="first-paragraph">
                        <p><?php echo nl2br($text['text']); ?></p>
                    </div>
                <?php endforeach; ?>
            <?php $featured_casinos_2 = get_field('featured_casinos_2'); ?>
            <?php foreach ($featured_casinos_2 as $casino) : ?>
                <div class="casino-bonus-box">
                    <div class="casino-box-logo-stars">
                        <div class="casino-bonus-box-logo">
                            <a href="<?php echo get_field('affialite_link', $casino->ID); ?>"> <img src="<?php echo get_field('logo', $casino->ID); ?>"></a>
                        </div>
                        <div class="casino-bonus-box-stars">
                            <p><?php echo $casino->post_title; ?></p>
                            <div class="star-ratings-2">
                                <div class="fill-ratings fill-ratings-2" style="width: <?php echo get_field('rating', $casino->ID); ?>%">
                                    <span>★★★★★</span>
                                </div>
                                <div class="empty-ratings empty-ratings-2">
                                    <span>☆☆☆☆☆</span>
                                </div>
                            </div>
                            <p><?php echo get_field('rating', $casino->ID); ?>/100</p>
                        </div>
                    </div>
                    <div class="casino-box-text-list">
                        <div class="casino-bonus-box-text">
                            <h3>CASINO FEATURES</h3>
                        </div>
                        <div class="casino-bonus-box-list-suk">
                            <?php foreach (get_field('tags', $casino->ID) as $tag) : ?>
                                <div class="casino-bonus-box-list">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-right" class="svg-inline--fa fa-chevron-circle-right fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm113.9 231L234.4 103.5c-9.4-9.4-24.6-9.4-33.9 0l-17 17c-9.4 9.4-9.4 24.6 0 33.9L285.1 256 183.5 357.6c-9.4 9.4-9.4 24.6 0 33.9l17 17c9.4 9.4 24.6 9.4 33.9 0L369.9 273c9.4-9.4 9.4-24.6 0-34z"></path></svg>
                                    <p><?php echo $tag['tag'] ?></p>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="casino-box-text-link">
                        <div class="casino-bonus-box-text2">
                            <h3>WELCOME BONUS</h3>
                        </div>
                        <div class="casino-bonus-box-button">
                            <h4><?php echo get_field('bonus', $casino->ID); ?></h4>
                            <p><?php echo get_field('free_spins', $casino->ID); ?></p>
                            <div class="visit-button2">
                                <a href="<?php echo get_field('affialite_link', $casino->ID); ?>" target="_blank">VISIT CASINO</a>
                            </div>
                            <h5><a href="<?php echo get_field('terms_link', $casino->ID); ?>" target="_blank">18+. T&C’s Apply</a></h5>
                        </div>
                    </div>
                </div>
                <?php if (!empty(get_field('terms_info', $casino->ID))) : ?>
                    <div class="casino-bonus-box-paragraph">
                        <p><?php echo get_field('terms_info', $casino->ID); ?></p>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <div class="casino-games-types-of-bonuses">
            <div class="types-of-bonuses">
                <?php $second_paragraph = get_field('second_paragraph'); ?>
                <?php foreach ($second_paragraph as $text) : ?>
                    <h2><?php echo $text['title']; ?></h2>
                    <div class="type-of-bonuses-paragraph">
                        <?php echo nl2br($text['text']); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="latest-casino-guides">
            <?php $guides_block_title = get_field('guides_block_title'); ?>
            <h2><?php echo $guides_block_title; ?></h2>
            <div class="latest-casino-guides-boxes">
               <?php $query = new WP_Query(array('posts_per_page' => 4)); ?>
                <?php $posts = $query->posts; ?>
                <?php foreach ($posts as $post) : ?>
                    <div class="latest-casino-guides-box">
                        <h3><?php echo $post->post_title; ?></h3>
                        <p><?php echo substr($post->post_content, 0, 200); ?>...</p>
                        <div class="visit-button3">
                            <a href="<?php echo get_permalink($post); ?>">READ GUIDE</a>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        <div class="casino-games-types-of-bonuses">
            <div class="types-of-bonuses">
                <?php $third_paragraph = get_field('third_paragraph'); ?>
                <?php foreach ($third_paragraph as $text) : ?>
                    <h2><?php echo $text['title']; ?></h2>
                    <div class="type-of-bonuses-paragraph">
                        <?php echo nl2br($text['text']); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
