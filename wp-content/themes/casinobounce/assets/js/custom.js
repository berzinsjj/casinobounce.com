jQuery(function ($) {
    jQuery(document).ready(function () {

        // Mobile menu
        $(".hamburger").click(function () {
            $(this).toggleClass("is-active");
            $(".menu-main-menu-container").toggleClass("is-active");
        });

        // Scroll to top
        $(".back-to-top a").click(function () {
            $("html, body").animate({scrollTop: 0}, 500);
            return false;
        });

        // Star rating
        var star_rating_width = $('.fill-ratings span').width();
        $('.star-ratings').width(star_rating_width);

        // Popup functions
        $(".open-popachka").click(function() {
            $(this).parent().parent().parent().find(".overlay").css("display", "block");
        })
        $(".close").click(function () {
            $(this).parent().parent().css("display", "none");
        });
    });
});

